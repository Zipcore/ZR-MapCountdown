
#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "ZR: Map Countdown"
#define PLUGIN_VERSION "1.3"
#define PLUGIN_DESCRIPTION "Adds a hint countdown based on map chat messages."
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

#include <multicolors>
#include <smlib>
#include <map_workshop_functions> // Adds workshop map support

#include <zombiereloaded>

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

int g_iMsgs;
Handle g_aMsg = null;
Handle g_aCountdown = null;

int g_iCountdown;

Handle g_hTimer = null;

int g_iInstructor[MAXPLAYERS + 1];
int g_iLastMsg[MAXPLAYERS + 1];

public void OnPluginStart()
{
	CreateConVar("zr_mapcountdown_version", PLUGIN_VERSION, "ZR: Map Countdown", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	AddCommandListener(Command_Say, "say");
	
	HookEvent("round_start", Event_RoundStart);
	HookEvent("round_end", Event_RoundEnd);
	
	CreateTimer(30.0, Timer_InstructorCheck, _, TIMER_REPEAT);
}

public void OnMapStart()
{
	ReadMapConfig();
}

public void OnMapEnd()
{
	g_hTimer = null;
}

public Action Event_RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	ResetCountdown();
	return Plugin_Continue;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	ResetCountdown();
	return Plugin_Continue;
}

public Action CS_OnTerminateRound(float &delay, CSRoundEndReason &reason)
{
	ResetCountdown();
	
	return Plugin_Continue;
}

public Action Command_Say(int client, char[] command, int args)
{
	if(client)
		return Plugin_Continue;
	
	char sMessage[512];
	GetCmdArgString(sMessage, 512);
	ProcessMessage(sMessage);
	
	return Plugin_Continue;
}

void ProcessMessage(char[] sMessage)
{
	//Prepare msg
	StripQuotes(sMessage);
	TrimString(sMessage);
	
	int iCountdown = GetCountdownForMessage(sMessage);
	
	// Add msg if not found
	if(iCountdown == -1)
	{
		iCountdown = MakeNumeric(sMessage);
		
		PushArrayString(g_aMsg, sMessage);
		PushArrayCell(g_aCountdown, iCountdown);
		g_iMsgs++;
		
		WriteMapConfig();
	}
	
	if(iCountdown > 0)
		StartCountdown(iCountdown);
}

void ResetCountdown()
{
	g_iCountdown = -1;
		
	if(g_hTimer != null)
		CloseHandle(g_hTimer);
		
	g_hTimer = null;
}

void StartCountdown(int iCountdown)
{
	if (iCountdown <= 0)
	{
		SetConVarInt(FindConVar("sv_gameinstructor_disable"), 1);
		return;
	}
	
	if(g_iCountdown < iCountdown)
	{
		g_iCountdown = iCountdown;
		
		if(g_hTimer != null)
			CloseHandle(g_hTimer);
			
		ShowHUD();
		g_hTimer = CreateTimer(1.0, Timer_Countdown, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action Timer_Countdown(Handle timer, any data)
{
	ShowHUD();
}

void ShowHUD()
{
	if(g_iCountdown < 0)
	{
		SetConVarInt(FindConVar("sv_gameinstructor_disable"), 1);
		return;
	}
		
	if(isWarmup())
	{
		g_iCountdown = -1;
		return;
	}
	
	SetConVarInt(FindConVar("sv_gameinstructor_disable"), 0);
	
	char centerText[512];
	char instructorText[100];
	
	if(g_iCountdown == 0)
	{
		Format(centerText, sizeof(centerText), "<font color='#ffff00' size='48'> GO! GO! GO!");
		Format(instructorText, sizeof(instructorText), "GO! GO! GO!");
		
		DisplayInstructorHint("admin_instructor_say", .text = instructorText, .timeout = 1, .color = {16, 133, 4}, .icon_onscreen = "icon_door", .icon_offscreen = "icon_door");
	}
	else 
	{
		Format(centerText, sizeof(centerText), "<font color='#ffff00' size='48'>         %d", g_iCountdown);
		Format(instructorText, sizeof(instructorText), "%d", g_iCountdown);
		
		DisplayInstructorHint("admin_instructor_say", .text = instructorText, .timeout = 1, .color = {255, 255, 0}, .icon_onscreen = "icon_door", .icon_offscreen = "icon_door");
	}
	
	g_iCountdown--;
	
	LoopAlivePlayers(client)
	{
		if(ZR_IsClientHuman(client))
		{
			if(g_iInstructor[client] == 0)
				PrintHintText(client, centerText);
		}
	}
}

int GetCountdownForMessage(char[] sMessage)
{
	for (int i = 0; i < g_iMsgs; i++)
	{
		char sBuffer[512];
		GetArrayString(g_aMsg, i, sBuffer, sizeof(sBuffer));
		
		if(StrEqual(sMessage, sBuffer))
			return GetArrayCell(g_aCountdown, i); // Msg found
	}
	
	return -1; // Fail
}

void ReadMapConfig()
{
	// Check Arrays
	
	g_iMsgs = 0;
	
	if(g_aMsg == null)
		g_aMsg = CreateArray(512);
	else ClearArray(g_aMsg);
	
	if(g_aCountdown == null)
		g_aCountdown = CreateArray(1);
	else ClearArray(g_aCountdown);
	
	// Open Config File
	
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/mapcountdown/%s.txt", sMap);
	
	Handle hFile = OpenFile(sPath, "r");
	
	char sBuffer[512];
	char sDatas[2][512];
	
	if (hFile != INVALID_HANDLE)
	{
		while (ReadFileLine(hFile, sBuffer, sizeof(sBuffer))) 
		{
			ExplodeString(sBuffer, ";", sDatas, 2, 512);
			
			StripQuotes(sDatas[1]);
			TrimString(sDatas[1]);
			
			if(!StrEqual(sDatas[1], ""))
			{
				PushArrayCell(g_aCountdown, StringToInt(sDatas[0]));
				PushArrayString(g_aMsg, sDatas[1]);
				
				g_iMsgs++;
			}
		}
		
		CloseHandle(hFile);
	}
}

void WriteMapConfig()
{
	char sRawMap[PLATFORM_MAX_PATH];
	char sMap[64];
	GetCurrentMap(sRawMap, sizeof(sRawMap));
	RemoveMapPath(sRawMap, sMap, sizeof(sMap));
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/mapcountdown/%s.txt", sMap);
	
	Handle hFile = OpenFile(sPath, "w");
	
	if (hFile != INVALID_HANDLE)
	{
		for (int i = 0; i < g_iMsgs; i++)
		{
			char sMessage[512];
			GetArrayString(g_aMsg, i, sMessage, sizeof(sMessage));
			
			WriteFileLine(hFile, "%d;%s;", GetArrayCell(g_aCountdown, i), sMessage);
		}
		
		CloseHandle(hFile);
	}
}

bool isWarmup()
{
	if (GameRules_GetProp("m_bWarmupPeriod") == 1)
		return true;
	return false;
}

int MakeNumeric(char[] sBuffer)
{
	char sBuffer2[32];
	int iCount;
	
	for (int i = 0; i < (strlen(sBuffer) - 1); i++)
	{
		if (IsCharNumeric(sBuffer[i]))
		{
			sBuffer2[iCount] = sBuffer[i];
			iCount++;
		}
	}
	
	return StringToInt(sBuffer2);
}

public Action Timer_InstructorCheck(Handle timer, any data)
{
	LoopIngameClients(client)
	{
		if(IsClientObserver(client))
			continue;
			
		if(IsFakeClient(client))
			continue;
		
		QueryClientConVar(client, "gameinstructor_enable", InstructorCheck, client);
	}
	
	return Plugin_Continue;	
}

public int InstructorCheck(QueryCookie cookie, int client, ConVarQueryResult result, const char[] cvarName, const char[] cvarValue)
{
	if (!client)
		return;
		
	if (!IsClientConnected(client))
		return;
	
	g_iInstructor[client] = StringToInt(cvarValue);
	
	int iTime = GetTime();
	
	if(g_iLastMsg[client] + 150 > iTime)
		return;
	
	g_iLastMsg[client] = iTime;
	
	if(g_iInstructor[client] == 0)
		CPrintToChat(client, "{grey2}[{darkred}FO{darkblue}TG{grey2}] {lime}You have {orchid}gameinstructor_enable 0{lime}. We really recommend that you change it back to {orchid}1{lime}!");
}

stock void RemoveEntity(entity, float time = 0.0)
{
    if (time == 0.0)
    {
        if (IsValidEntity(entity))
        {
            char edictname[32];
            GetEdictClassname(entity, edictname, 32);

            if (!StrEqual(edictname, "player"))
                AcceptEntityInput(entity, "kill");
        }
    }
    else if(time > 0.0)
        CreateTimer(time, RemoveEntityTimer, EntIndexToEntRef(entity), TIMER_FLAG_NO_MAPCHANGE);
}

public Action RemoveEntityTimer(Handle Timer, any entityRef)
{
    int entity = EntRefToEntIndex(entityRef);
    if (entity != INVALID_ENT_REFERENCE)
        RemoveEntity(entity); // RemoveEntity(...) is capable of handling references
    
    return (Plugin_Stop);
}

/*
	Hint flags:
	
	0 - Nothing
	1 - Slow Size Pulse
	2 - Fast Size Pulse
	4 - Urgend Size Pulse
	8 - Slow Alpha Pulse
	16 - Fast Alpha Pulse
	32 - Urgent Alpha Pulse
	64 - Shake Narrow
	128 - Wide Shake
	
	Available icons:
	
    "icon_bulb"
    "icon_caution"
    "icon_alert"
    "icon_alert_red"
    "icon_tip"
    "icon_skull"
    "icon_no"
    "icon_run"
    "icon_interact"
    "icon_button"
    "icon_door"
    "icon_arrow_plain"
    "icon_arrow_plain_white_dn"
    "icon_arrow_plain_white_up"
    "icon_arrow_up"
    "icon_arrow_right"
    "icon_fire"
    "icon_present"
    "use_binding"
*/

stock void DisplayInstructorHint(char name[64] = "Hint",
								 char key[64] = "Key",
								 int target = 0,
								 char text[100] = "",
								 int activator = 0,
								 char activator_text[100] = "",
								 int timeout = 5,
								 int color[3] = {255, 255, 255},
								 float icon_height = 0.0,
								 char[] icon_onscreen = "icon_tip",
								 char[] icon_offscreen = "icon_tip",
								 float range = 0.0,
								 char binding[100] = "",
								 bool allow_nodraw_target = true,
								 bool nooffscreen = false,
								 bool forcecaption = true,
								 bool local_player_only = false,
								 int flag = 0)
{  
	Event event = CreateEvent("instructor_server_hint_create");
	if (event == INVALID_HANDLE)
	{
		return;
	}
	
	event.SetString("hint_name", name);
	event.SetString("hint_replace_key", key);
	
	event.SetInt("hint_target", target);									//Target
	
	event.SetString("hint_caption", text);									//Message
	
	event.SetInt("hint_activator_userid", activator);						//Activator
	event.SetString("hint_activator_caption", activator_text);						//Activator Message
	
	event.SetInt("hint_timeout", timeout);								//Time
	char sColotBuffer[32];
	FormatEx(sColotBuffer, sizeof(sColotBuffer), "%d,%d,%d", color[0], color[1], color[2]);
	event.SetString("hint_color", sColotBuffer);
	
	event.SetFloat("hint_icon_offset", icon_height);
	event.SetString("hint_icon_onscreen", icon_onscreen);					//Icon
	event.SetString("hint_icon_offscreen", icon_offscreen);					//Icon
	
	event.SetFloat("hint_range", range);
	event.SetString("hint_binding", binding);
	event.SetBool("hint_allow_nodraw_target", allow_nodraw_target);
	event.SetBool("hint_nooffscreen", nooffscreen);
	event.SetBool("hint_forcecaption", forcecaption);
	event.SetBool("hint_local_player_only", local_player_only);
	event.SetInt("hint_flags", flag);
	
	event.Fire();
}